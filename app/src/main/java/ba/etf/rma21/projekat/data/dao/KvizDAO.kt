package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Kviz

@Dao
interface KvizDAO {
    @Query("SELECT * FROM kviz")
    suspend fun getKviz(): List<Kviz>

    @Query("DELETE FROM kviz")
    suspend fun deleteAllFromKviz()

    @Insert
    suspend fun insertKviz(vararg kviz: Kviz)
}