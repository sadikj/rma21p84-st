package ba.etf.rma21.projekat.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz

class KvizListAdapter(
    private var kvizovi: List<Kviz>,
    private val onItemClicked: (movie:Kviz) -> Unit
) : RecyclerView.Adapter<KvizListAdapter.KvizViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KvizViewHolder
    {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.kviz_layout, parent, false)
        return KvizViewHolder(view)
    }
    override fun getItemCount(): Int = kvizovi.size
    override fun onBindViewHolder(holder: KvizViewHolder, position: Int) {
        //val formatDatuma: DateFormat = SimpleDateFormat("dd.MM.yyyy")
        val context: Context = holder.slikaAktivnosti.getContext()
        holder.brojBodova.text = ""
        if (kvizovi[position].stanje.equals("zavrsen")){
            var id: Int = context.getResources().getIdentifier("plava", "drawable", context.getPackageName())
            holder.slikaAktivnosti.setImageResource(id)
            holder.brojBodova.text = kvizovi[position].osvojeniBodovi.toString()
            val datumSplitan = kvizovi[position].datumRada?.split("T")
            val splitPoCrtici = datumSplitan!![0].split("-")
            val stringDatum = splitPoCrtici[2]+"."+splitPoCrtici[1]+"."+splitPoCrtici[0]
            holder.datumKviz.text = stringDatum
            //holder.datumKviz.text = formatDatuma.format(kvizovi[position].datumRada)
        } else if (kvizovi[position].stanje.equals("prosli")) {
            var id: Int = context.getResources().getIdentifier("crvena", "drawable", context.getPackageName())
            holder.slikaAktivnosti.setImageResource(id)
            val datumSplitan = kvizovi[position].datumKraj?.split("T")
            val splitPoCrtici = datumSplitan!![0].split("-")
            val stringDatum = splitPoCrtici[2]+"."+splitPoCrtici[1]+"."+splitPoCrtici[0]
            holder.datumKviz.text = stringDatum
            //holder.datumKviz.text = formatDatuma.format(kvizovi[position].datumKraj)
        } else if (kvizovi[position].stanje.equals("buduci")) {
            var id: Int = context.getResources().getIdentifier("zuta", "drawable", context.getPackageName())
            holder.slikaAktivnosti.setImageResource(id)
            val datumSplitan = kvizovi[position].datumPocetka.split("T")
            val splitPoCrtici = datumSplitan[0].split("-")
            val stringDatum = splitPoCrtici[2]+"."+splitPoCrtici[1]+"."+splitPoCrtici[0]
            holder.datumKviz.text = stringDatum
            //holder.datumKviz.text = formatDatuma.format(kvizovi[position].datumPocetka)
        } else if (kvizovi[position].stanje.equals("aktivan")){
            var id: Int = context.getResources().getIdentifier("zelena", "drawable", context.getPackageName())
            holder.slikaAktivnosti.setImageResource(id)
            val datumSplitan = kvizovi[position].datumKraj?.split("T")
            val splitPoCrtici = datumSplitan!![0].split("-")
            val stringDatum = splitPoCrtici[2]+"."+splitPoCrtici[1]+"."+splitPoCrtici[0]
            holder.datumKviz.text = stringDatum

            //holder.datumKviz.text = formatDatuma.format(kvizovi[position].datumKraj)
        }
        /*else if(kvizovi[position].datumKraj!!.before(Date())){
            /*if(kvizovi[position].datumRada==null) {
                var id: Int = context.getResources().getIdentifier("crvena", "drawable", context.getPackageName())
                holder.slikaAktivnosti.setImageResource(id)
                holder.datumKviz.text = formatDatuma.format(kvizovi[position].datumKraj)
            }
            else{
                var id: Int = context.getResources().getIdentifier("plava", "drawable", context.getPackageName())
                holder.slikaAktivnosti.setImageResource(id)
                holder.brojBodova.text = kvizovi[position].osvojeniBodovi.toString()
                holder.datumKviz.text = formatDatuma.format(kvizovi[position].datumRada)
            }*/
        }
        else if(kvizovi[position].datumPocetka.before(Date()) && kvizovi[position].datumKraj!!.after(Date())){
            var id: Int = context.getResources().getIdentifier("zelena", "drawable", context.getPackageName())
            holder.slikaAktivnosti.setImageResource(id)
            holder.datumKviz.text = formatDatuma.format(kvizovi[position].datumKraj)
        }
        else{
            var id: Int = context.getResources().getIdentifier("zuta", "drawable", context.getPackageName())
            holder.slikaAktivnosti.setImageResource(id)
            holder.datumKviz.text = formatDatuma.format(kvizovi[position].datumPocetka)
        }*/

        holder.nazivPredmeta.text = kvizovi[position].nazivPredmeta
        holder.naziv.text = kvizovi[position].naziv
        holder.vremenskoTrajanje.text = kvizovi[position].trajanje.toString() + " min"

        holder.itemView.setOnClickListener{ onItemClicked(kvizovi[position])}


    }
    fun updateKviz(kvizovi: List<Kviz>) {
        this.kvizovi = kvizovi
        notifyDataSetChanged()
    }

    inner class KvizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nazivPredmeta : TextView = itemView.findViewById(R.id.nazivPredmeta)
        val slikaAktivnosti : ImageView = itemView.findViewById(R.id.slikaAktivnosti)
        val naziv : TextView = itemView.findViewById(R.id.naziv)
        val datumKviz : TextView = itemView.findViewById(R.id.datumKviz)
        val vremenskoTrajanje : TextView = itemView.findViewById(R.id.vremenskoTrajanje)
        val brojBodova : TextView = itemView.findViewById(R.id.brojBodova)
    }
}