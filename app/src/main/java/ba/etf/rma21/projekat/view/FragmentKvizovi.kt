package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.repositories.*
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class FragmentKvizovi : Fragment(){
    private lateinit var listaKvizova: RecyclerView
    private lateinit var listaKvizovaAdapter: KvizListAdapter
    private var kvizListViewModel =  KvizListViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view =  inflater.inflate(R.layout.fragment_kviz, container, false)

        postaviBottom(true, true, false, false)
/**/
        listaKvizova = view.findViewById(R.id.listaKvizova)
        val gridView = GridLayoutManager(view.context, 2, GridLayoutManager.VERTICAL, false)
        listaKvizova.layoutManager=gridView
        listaKvizovaAdapter = KvizListAdapter(listOf()){ kviz -> showAllQuestions(kviz) }
        listaKvizova.adapter = listaKvizovaAdapter
        //listaKvizovaAdapter.updateKviz(KvizListViewModel().getMyKvizes(onSuccess =:: onSuccess, onError=::onError))
        KvizListViewModel().getMyKvizes(onSuccess =:: onSuccess, onError=::onError)
        /*GlobalScope.launch (Dispatchers.Main) {
            listaKvizovaAdapter.updateKviz(KvizListViewModel().getMyKvizes1())
        }*/

        val spinner : Spinner = view.findViewById(R.id.filterKvizova)
        ArrayAdapter.createFromResource(
            view.context,
            R.array.Spinner,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
        }

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(spinner.getItemAtPosition(position).equals("Svi kvizovi")){
                    /*GlobalScope.launch (Dispatchers.Main) {
                        listaKvizovaAdapter.updateKviz(kvizListViewModel.getAllKviz1())
                    }*/
                    KvizListViewModel().getAllKviz(onSuccess =:: onSuccess, onError=::onError)
                }
                else if(spinner.getItemAtPosition(position).equals("Svi moji kvizovi")){
                    /*GlobalScope.launch (Dispatchers.Main) {
                        listaKvizovaAdapter.updateKviz(kvizListViewModel.getMyKvizes1())
                    }*/
                    KvizListViewModel().getMyKvizes(onSuccess =:: onSuccess, onError=::onError)
                }
                else if(spinner.getItemAtPosition(position).equals("Urađeni kvizovi")){
                    /*GlobalScope.launch (Dispatchers.Main) {
                        listaKvizovaAdapter.updateKviz(kvizListViewModel.getDone1())
                    }*/
                    KvizListViewModel().getDone(onSuccess =:: onSuccess, onError=::onError)
                }
                else if(spinner.getItemAtPosition(position).equals("Budući kvizovi")){
                    /*GlobalScope.launch (Dispatchers.Main) {
                        listaKvizovaAdapter.updateKviz(kvizListViewModel.getFuture1())
                    }*/
                    KvizListViewModel().getFuture(onSuccess =:: onSuccess, onError=::onError)
                }
                else if(spinner.getItemAtPosition(position).equals("Prošli kvizovi")){
                    /*GlobalScope.launch (Dispatchers.Main) {
                        listaKvizovaAdapter.updateKviz(kvizListViewModel.getNotTaken1())
                    }*/
                    KvizListViewModel().getNotTaken(onSuccess =:: onSuccess, onError=::onError)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                /*GlobalScope.launch (Dispatchers.Main) {
                    listaKvizovaAdapter.updateKviz(kvizListViewModel.getAllKviz())
                }*/
                KvizListViewModel().getAllKviz(onSuccess =:: onSuccess, onError=::onError)
            }

        }

        return view
    }

    private fun showAllQuestions(kviz1: Kviz) {
        GlobalScope.launch (Dispatchers.Main) {
//            val patternDate = "yyyy-MM-dd HH:mm:ss"
//            val simpleDateFormat = SimpleDateFormat(patternDate)
//            var date123 = simpleDateFormat.format(Date()).split(" ")[0]+"T"+simpleDateFormat.format(Date()).split(" ")[1]
//            kviz1.datumRada = date123
//            kviz1.stanje="zavrsen"
            provjera = false
            vidljivo = false
            kviz1Glo = kviz1


            var postojiMjesto = false
            var listaPitanja = PitanjeKvizRepository.dohvatiSvaPitanjaIzBazePodatakaZaJedanKviz(kviz1.id)!!

            var listagetKvizTekena = TakeKvizRepository.getPocetiKvizovi()
            if(listagetKvizTekena!=null){
                for (elIzListe in listagetKvizTekena) {
                    if(elIzListe.KvizId == kviz1.id){
                        postojiMjesto = true
                        idKvizaTakena = elIzListe
                        break
                    }
                }
            }
            if(postojiMjesto){
                postaviBottom(false, false, true, true)
                vidljivo = true
                //idKvizaTakena = TakeKvizRepository.zapocniKviz(kviz1.id)!!
                val nextFrag = FragmentPokusaj(listaPitanja)
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.kontener, nextFrag)
                    .addToBackStack(null)
                    .commit()
            }
            else {
                vidljivo = false
                val mojiKvizovi = KvizRepository.getMyKvizes()
                for(el in mojiKvizovi){
                    if(el.id == kviz1.id){
                        postaviBottom(false, false, true, true)
                        idKvizaTakena = TakeKvizRepository.zapocniKviz(kviz1.id)!!
                        val nextFrag = FragmentPokusaj(listaPitanja)
                        requireActivity().supportFragmentManager.beginTransaction()
                            .replace(R.id.kontener, nextFrag)
                            .addToBackStack(null)
                            .commit()
                    }
                    else{
                       // postaviBottom(true, true, false, false)
                    }
                }
            }
/*
            var pitanjeKvizOdgovorVar = PitanjeKvizRepository.getPitanja(kviz1.id)!!



            for (el in pitanjeKvizOdgovorVar) {
                if (kviz1 == el.kviz1) {
                    postojiMjesto = true
                }
            }*/
           // var listaZaPoslat = PitanjeKvizRepository.getPitanja(kviz1.id)



          //  var listaOdgovoraReset = FragmentPokusaj.getListaOdgovora()

          //  listaOdgovoraReset.clear()

          //  var mojiKvizovi = false
             //   var listaMojihKvizova = kvizListViewModel.getMyKvizes1()


              //  for (elLitse in listaMojihKvizova) {
              //      if(elLitse == kviz1){
              //          mojiKvizovi = true
              //      }
             //   }

           // if (mojiKvizovi) {


              //  if (!postojiMjesto) {
                  //  FragmentPitanje.procenatTacnosti = 0

                  //  var listaPitanjaKviz = PitanjeKvizRepository.getPitanja(kviz1.id)!!


                   // var odgovori = mutableListOf<Int>()

                   // listaPitanjaBroj = listaZaPoslat!!.size
/*
                    if (pitanjeKvizOdgovorVar != null) {
                        for (pitanje in pitanjeKvizOdgovorVar) {
                            /*if (pitanje.kviz1 == kviz1) {
                            odgovori.addAll(pitanje.listaOdgovora)
                        }*/
                        }
                    }*/

                   // for (el in odgovori) {
                    //    if (el != -1) {
                    //        provjera = true
                    //    }
                  //  }

                   // listaOdgovoraReset.addAll(odgovori)

                    //val nextFrag = FragmentPokusaj(listaZaPoslat)
                //requireActivity().supportFragmentManager.beginTransaction()
                   //     .replace(R.id.kontener, nextFrag)
                  //      .addToBackStack(null)
                  //      .commit()

                    // } else {
                    //   vidljivo = true
                    //  var odgovori = mutableListOf<Int>()
                    //if (pitanjeKvizOdgovorVar != null) {
                    //    for (pitanje in pitanjeKvizOdgovorVar) {
                            /*if (pitanje.kviz1 == kviz1) {
                            for (el in pitanje.listaOdgovora) {
                                if (el == -1) {
                                    odgovori.add(-2)
                                } else {
                                    odgovori.add(el)
                                }
                            }
                        }*/
                    //    }
                    //}
                    //listaOdgovoraReset.addAll(odgovori)
                    //postaviBottom(true, true, false, false)
                    //val nextFrag = FragmentPokusaj(listaZaPoslat!!)
                        //requireActivity().supportFragmentManager.beginTransaction()
                        //.replace(R.id.kontener, nextFrag)
                        //.addToBackStack(null)
                    //.commit()
                    // }
                    //} else {
                    //val nextFrag = FragmentPokusaj(PitanjeKvizRepository.getPitanja(kviz1.id)!!)
                        //requireActivity().supportFragmentManager.beginTransaction()
                        // .replace(R.id.kontener, nextFrag)
                        // .addToBackStack(null)
                    // .commit()
                    //}
        }
    }

    companion object {
        lateinit var kviz1Glo : Kviz
        lateinit var idKvizaTakena : KvizTaken
        var provjera : Boolean = false
        var vidljivo : Boolean = false
        var listaPitanjaBroj : Int = 0
        fun newInstance(): FragmentKvizovi = FragmentKvizovi()
    }

    fun onSuccess(kviz:List<Kviz>){

        listaKvizovaAdapter.updateKviz(kviz)
    }
    fun onError() {
    }

    fun onBackPressed() {
        val fragment = FragmentPredmeti.newInstance()
        val transaction = parentFragmentManager.beginTransaction()
        transaction.replace(R.id.kontener, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun postaviBottom(b1: Boolean, b2: Boolean, b3: Boolean, b4: Boolean){
        var navigacija : BottomNavigationView = requireActivity().findViewById(R.id.bottomNav)
        navigacija.getMenu().findItem(R.id.predmeti).isVisible=b1
        navigacija.getMenu().findItem(R.id.kvizovi).isVisible=b2
        navigacija.getMenu().findItem(R.id.predajKviz).isVisible=b3
        navigacija.getMenu().findItem(R.id.zaustaviKviz).isVisible=b4
    }

}