package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.*

import ba.etf.rma21.projekat.data.repositories.PredmetIGrupaRepository


class UpisPredmetViewModel {
    suspend fun getGroupsByPredmet(idPredmet: Int): List<Grupa>? {
        return PredmetIGrupaRepository.getGrupeZaPredmet(idPredmet)
    }
    /*fun getUpisani(): List<Predmet1> {
        return PredmetRepository1.getUpisani()
    }*/
    /*fun getAll(): List<Predmet1>{
        return PredmetRepository1.getAll()
    }*/
    /*fun getPredmetsByGodina(godina:Int) : List<Predmet1>{
        return PredmetRepository1.getPredmetsByGodina(godina)
    }*/
    suspend fun getPredmetsNotEnrolled(godina: Int): List<Predmet>{
        return PredmetIGrupaRepository.getPredmetsNotEnrolled(godina)
    }
    /*fun setNewKorisnik(predmet :Predmet1, grupa1: Grupa1) {
        return KorisnikRepository1.setNewKorisnik(predmet, grupa1)
    }
    fun getAllKorsinik() : List<Korisnik1>{
        return KorisnikRepository1.getAllKorsinik()
    }*/

    suspend fun upisiGrupu(idGrupa:Int){
        PredmetIGrupaRepository.upisiUGrupu(idGrupa)
    }
}