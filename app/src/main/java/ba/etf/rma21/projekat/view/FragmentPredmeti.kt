package ba.etf.rma21.projekat.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.core.view.isEmpty
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.viewmodel.UpisPredmetViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class FragmentPredmeti : Fragment() {

    private val listaPredmeta : ArrayList<String> = ArrayList()
    private val listaGrupa : ArrayList<String> =  ArrayList()
    private var upisPredmetaViewModel =  UpisPredmetViewModel()
    private var predmeti = emptyList<Predmet>()
    private var sveGrupe = emptyList<Grupa>()


    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.activity_upis_predmet, container, false)
        //var listaKorisnika = upisPredmetaViewModel.getAllKorsinik()
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_upis_predmet)

        val odabirGodina : Spinner = view.findViewById(R.id.odabirGodina)
        val odabirPredmet : Spinner = view.findViewById(R.id.odabirPredmet)
        val odabirGrupa : Spinner = view.findViewById(R.id.odabirGrupa)
        val dodajPredmetDugme : Button = view.findViewById(R.id.dodajPredmetDugme)

        val arrayList: ArrayList<String> = ArrayList()
        arrayList.add("1")
        arrayList.add("2")
        arrayList.add("3")
        arrayList.add("4")
        arrayList.add("5")
        val arrayAdapter = ArrayAdapter(
                view.context,
                android.R.layout.simple_spinner_item,
                arrayList
        )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        odabirGodina.setAdapter(arrayAdapter)

        odabirGodina.setSelection(selektovanaGodina)

        odabirGodina.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View?,
                    position: Int,
                    id: Long
            ) {
                GlobalScope.launch (Dispatchers.Main) {

                    listaPredmeta.clear()

                    predmeti =
                        upisPredmetaViewModel.getPredmetsNotEnrolled(parent.selectedItemPosition + 1)

                    for (lista in predmeti) {
                        listaPredmeta.add(lista.naziv)
                    }

                    if (view != null) {
                        popuniSpiner(view.context, odabirPredmet, listaPredmeta)
                    }

                    if (odabirPredmet != null && odabirPredmet.getSelectedItem() != null) {
                        dodajPredmetDugme.isEnabled = true
                    } else {
                        dodajPredmetDugme.isEnabled = false
                        listaGrupa.clear()
                        if (view != null) {
                            popuniSpiner(view.context, odabirGrupa, listaGrupa)
                        }
                    }
                    selektovanaGodina = odabirGodina.selectedItemPosition
                    odabirPredmet.setSelection(selektovaniPredmet)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        })

        odabirPredmet.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                    parentView: AdapterView<*>,
                    selectedItemView: View,
                    position: Int,
                    id: Long
            ) {
                GlobalScope.launch (Dispatchers.Main) {
                    listaGrupa.clear()

                    if (!odabirPredmet.isEmpty()) {

                        sveGrupe =
                            upisPredmetaViewModel.getGroupsByPredmet(predmeti[odabirPredmet.selectedItemPosition].id)!!
                        for (lista in sveGrupe) {
                            listaGrupa.add(lista.naziv)
                        }

                        popuniSpiner(view.context, odabirGrupa, listaGrupa)

                    }

                    dodajPredmetDugme.isEnabled =
                        odabirGrupa != null && odabirGrupa.getSelectedItem() != null
                    selektovaniPredmet = odabirPredmet.selectedItemPosition
                    odabirGrupa.setSelection(selektovanaGrupa)
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here
            }
        })
        odabirGrupa.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View, position: Int, id: Long) {
                selektovanaGrupa = odabirGrupa.selectedItemPosition
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here
            }
        })


        dodajPredmetDugme.setOnClickListener{
            GlobalScope.launch (Dispatchers.Main) {
                val bundle = Bundle()
                val godina = odabirGodina.selectedItemPosition + 1
                val predmet = odabirPredmet.selectedItem.toString()
                val grupa = odabirGrupa.selectedItem.toString()
                //upisPredmetaViewModel.setNewKorisnik(Predmet1(predmet, godina), Grupa1(grupa, predmet))
                upisPredmetaViewModel.upisiGrupu(sveGrupe[odabirGrupa.selectedItemPosition].id)

                bundle.putString(
                    "kljucPredmet",
                    "Uspješno ste upisani u grupu ${grupa} predmeta ${predmet}!"
                )

                val nextFrag = FragmentPoruka()
                nextFrag.arguments = bundle
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.kontener, nextFrag)
                    .addToBackStack(null)
                    .commit()
            }

        }
        return view
    }

    private fun popuniSpiner(context: Context, odabir: Spinner, lista: ArrayList<String>) {
        val predmetAdapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, lista)
        predmetAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        odabir.setAdapter(predmetAdapter)
    }

    companion object {
        fun newInstance(): FragmentPredmeti = FragmentPredmeti()
        var selektovanaGodina: Int = 0
        var selektovaniPredmet : Int = 0
        var selektovanaGrupa : Int = 0
    }

    fun onBackPressed() {
        val fragment = FragmentKvizovi.newInstance()
        val transaction = parentFragmentManager.beginTransaction()
        transaction.replace(R.id.kontener, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}