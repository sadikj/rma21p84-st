package ba.etf.rma21.projekat.data.dao

import ba.etf.rma21.projekat.data.models.Account
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface AccountDAO {
    @Query("SELECT * FROM account")
    suspend fun getAccount(): Account
    @Query("UPDATE account SET lastUpdate = :lastUpdate WHERE acHash = :acHash")
    fun updateDateOnAccount(acHash: String, lastUpdate: String)
    @Insert
    suspend fun insertAccount(vararg account: Account)
}