package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R

class FragmentPoruka : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_poruka, container, false)
        val tvPoruka : TextView = view.findViewById(R.id.tvPoruka)
        tvPoruka.text = this.arguments?.getString("kljucPredmet")
        return view
    }

    companion object {
        fun newInstance(): FragmentPoruka = FragmentPoruka()
    }
    fun onBackPressed() {
        val fragment = FragmentKvizovi.newInstance()
        val transaction = parentFragmentManager.beginTransaction()
        transaction.replace(R.id.kontener, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}