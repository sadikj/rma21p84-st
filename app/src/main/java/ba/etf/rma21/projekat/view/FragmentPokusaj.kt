package ba.etf.rma21.projekat.view

import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class FragmentPokusaj(pitanja: List<Pitanje>) : Fragment(){

    var pitanja : List<Pitanje> = pitanja
    lateinit var navigacijaPitanja : NavigationView
    var kvizTaken : KvizTaken = FragmentKvizovi.idKvizaTakena
    var kviz : Kviz = FragmentKvizovi.kviz1Glo

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_pokusaj, container, false)
        navigacijaPitanja = view.findViewById(R.id.navigacijaPitanja)
        val menuView: Menu = navigacijaPitanja.getMenu()
        var i : Int = 1
        for(pitanje in pitanja){
            menuView.add(i - 1, i - 1, i - 1, i.toString())
            menuView.getItem(i - 1).isCheckable = true
            listaOdgovoraKorisnika.add(-1)
            i += 1
        }
        menuView.add(i-1,i-1,i-1,"Rezultat")

        menuView[i-1].isVisible = FragmentKvizovi.vidljivo

        navigacijaPitanja.post {
            GlobalScope.launch(Dispatchers.Main) {
                val listOdg = OdgovorRepository.dohvatiSveOdgovoreZaKvizTakenIzBazePodataka(kviz.id)
                if (listOdg != null) {
                    for (i in pitanja.indices) {
                        for (j in listOdg) {
                            if (pitanja[i].id==j.PitanjeId && kvizTaken.id==j.KvizTakenId) {
                                if (j.odgovoreno == pitanja[i].tacan) {
                                    /**/
                                    val bojaTExtaPozadine = SpannableString(
                                        navigacijaPitanja?.menu?.getItem(i)?.getTitle().toString()
                                    )
                                    bojaTExtaPozadine.setSpan(
                                        ForegroundColorSpan(
                                            ContextCompat.getColor(
                                                requireContext(),
                                                R.color.zelena
                                            )
                                        ), 0, bojaTExtaPozadine.length, 0
                                    )
                                    navigacijaPitanja?.menu?.getItem(i)?.title = bojaTExtaPozadine
                                } else {
                                    val bojaTExtaPozadine = SpannableString(navigacijaPitanja?.menu?.getItem(i)?.getTitle().toString())
                                    bojaTExtaPozadine.setSpan(ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.crvena)), 0, bojaTExtaPozadine.length, 0)
                                    navigacijaPitanja?.menu?.getItem(i)?.title = bojaTExtaPozadine
                                }
                                break
                            }
                        }
                    }
                }
            }
        }

        /*navigacijaPitanja.post {
            if (pitanja.isNotEmpty()) {
                GlobalScope.launch (Dispatchers.Main) {
                    var listaOdg = OdgovorRepository.getOdgovoriKviz(kviz.id)
                    if (listaOdg != null) {
                        for(pitanje1 in pitanja){
                        for (elListe in listaOdg) {
                            if (elListe.PitanjeId == pitanje1.id && elListe.KvizTakenId == kvizTaken.id) {
                                if (elListe.odgovoreno == pitanje1.tacan) {
                                    val bojaTExtaPozadine = SpannableString(navigacijaPitanja?.menu?.getItem(pitanje1.id)?.getTitle().toString())
                                    bojaTExtaPozadine.setSpan(ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.zelena)), 0, bojaTExtaPozadine.length, 0)
                                    navigacijaPitanja?.menu?.getItem(pitanje1.id)?.title = bojaTExtaPozadine
                                } else {

                                }
                            }
                        }
                    }
                    }
                }
                //var listAll = PitanjeKvizRepository1.listaSvega

                //var kvizGlobalan = FragmentKvizovi.kviz1Glo

                //var brojacEl = 0
                /*for (el in listAll) {
                    if (kvizGlobalan == el.kviz1) {
                        break
                    }
                    brojacEl++
                }*/

                //BOJI PRI UCITANJU KVIZA
                /*for (elRasporedOdgovor in listAll[brojacEl].rasporedBiranja!!) {
                    if (listAll.get(brojacEl).listaOdgovora[elRasporedOdgovor] != -1) {
                        if (listAll.get(brojacEl).listaOdgovora.get(elRasporedOdgovor) == pitanja[elRasporedOdgovor].tacan) {
                            val bojaTExtaPozadine = SpannableString(navigacijaPitanja?.menu?.getItem(elRasporedOdgovor)?.getTitle().toString())
                            bojaTExtaPozadine.setSpan(ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.zelena)), 0, bojaTExtaPozadine.length, 0)
                            navigacijaPitanja?.menu?.getItem(elRasporedOdgovor)?.title = bojaTExtaPozadine
                        } else {
                            val bojaTExtaPozadine = SpannableString(navigacijaPitanja?.menu?.getItem(elRasporedOdgovor)?.getTitle().toString())
                            bojaTExtaPozadine.setSpan(ForegroundColorSpan(ContextCompat.getColor(requireContext(), R.color.crvena)), 0, bojaTExtaPozadine.length, 0)
                            navigacijaPitanja?.menu?.getItem(elRasporedOdgovor)?.title = bojaTExtaPozadine
                        }
                    }
                }*/
            }
        }*/

        navigacijaPitanja.setNavigationItemSelectedListener { item ->
            spasiBroj = item.itemId
            if(item.title == "Rezultat"){
                val nextFrag = FragmentPoruka()
                val bundle = Bundle()
                val kviz = FragmentKvizovi.kviz1Glo
                //POTREBNO UZETI SVA PITANJA
                //var pitanjeKvizOdgovorVar = PitanjeKvizRepository1.listaSvega
                var procentTacnost1 :Int = 0
                /*for(pitanje in pitanjeKvizOdgovorVar){
                    if(pitanje.kviz1 == kviz){
                        procentTacnost1 = pitanje.brojTacnih
                    }
                }*/

                bundle.putString("kljucPredmet", "Završili ste kviz ${kviz.naziv} sa tačnosti ${kviz.osvojeniBodovi}")
                nextFrag.arguments = bundle
                childFragmentManager.beginTransaction()
                        .replace(R.id.framePitanje, nextFrag)
                        .addToBackStack(null)
                        .commit()
            }
            else{
                pitanje(item.itemId)

            }
            false
        }

        return view
    }

    private fun pitanje(i: Int) {

        //var listaPitanjeKvizOdgovo = PitanjeKvizRepository1.listaSvega
        var kvizGlobal = FragmentKvizovi.kviz1Glo

        var brojac = 0
        /*if(listaOdgovoraKorisnika!=null) {
            for (elListe in listaPitanjeKvizOdgovo) {
                if (elListe.kviz1 == kvizGlobal){
                    break
                }
                brojac++
            }
        }*/

        //listaPitanjeKvizOdgovo[brojac].rasporedBiranja?.add(i)

        //var listaOd = listaPitanjeKvizOdgovo[brojac].listaOdgovora
        GlobalScope.launch (Dispatchers.Main) {
            val nextFrag = FragmentPitanje(pitanja.get(i))
            childFragmentManager.beginTransaction()
                .replace(R.id.framePitanje, nextFrag)
                .addToBackStack(null)
                .commit()
        }
    }

    companion object {
        var spasiBroj = 0
        var disablePitanje = false
        var listaOdgovoraKorisnika : MutableList<Int> = mutableListOf()
        fun newInstance(pitanja: List<Pitanje>): FragmentPokusaj = FragmentPokusaj(pitanja)
        fun getListaOdgovora() : MutableList<Int>{
            return listaOdgovoraKorisnika
        }
    }
}
