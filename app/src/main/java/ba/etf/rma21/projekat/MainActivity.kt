package ba.etf.rma21.projekat

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.*
import ba.etf.rma21.projekat.view.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*


class MainActivity : AppCompatActivity() {

    private lateinit var bottomNavigation: BottomNavigationView
    private val br: BroadcastReceiver = ConnectivityBroadcastReceiver()
    private val filter = IntentFilter("android.net.conn.CONNECTIVITY_CHANGE")

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.kvizovi -> {
                val favoritesFragment = FragmentKvizovi.newInstance()
                openFragment(favoritesFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.predmeti -> {
                val recentFragments = FragmentPredmeti.newInstance()
                openFragment(recentFragments)
                return@OnNavigationItemSelectedListener true
            }
            R.id.predajKviz -> {
                GlobalScope.launch (Dispatchers.Main) {
                    AccountRepository.postaviHash(AccountRepository.acHash)
                    PredmetIGrupaRepository.upisiGrupuUBazuPodataka()
                    PredmetIGrupaRepository.upisiPredmetUBazuPodataka()
                    KvizRepository.upisiKvizUBazuPodataka()
                    PitanjeKvizRepository.upisiPitanjeUBazuPodataka()

                    var navigacija: BottomNavigationView = findViewById(R.id.bottomNav)
                    navigacija.getMenu().findItem(R.id.predmeti).isVisible = true
                    navigacija.getMenu().findItem(R.id.kvizovi).isVisible = true
                    navigacija.getMenu().findItem(R.id.predajKviz).isVisible = false
                    navigacija.getMenu().findItem(R.id.zaustaviKviz).isVisible = false

                    val recentFragments = FragmentPoruka.newInstance()
                    val bundle = Bundle()
                    val kviz = FragmentKvizovi.kviz1Glo

                    //var pitanjeKvizOdgovorVar = PitanjeKvizRepository1.listaSvega
                    //var procentTacnost: Double = FragmentPitanje.procenatTacnosti.toDouble()
                    /*for(pitanje in pitanjeKvizOdgovorVar){
                    if(pitanje.kviz1 == kviz){
                        pitanje.brojTacnih = FragmentPitanje.procenatTacnosti
                    }
                }*/
                    /*var brojPitanja = FragmentKvizovi.listaPitanjaBroj
                procentTacnost = (procentTacnost / brojPitanja) * 100
                procentTacnost = Math.round(procentTacnost * 100.0) / 100.0*/

                    //kviz.datumRada = Date()
                    //kviz.osvojeniBodovi = procentTacnost.toFloat()
                    bundle.putString(
                        "kljucPredmet",
                        "Završili ste kviz ${kviz.naziv} sa tačnosti ${kviz.osvojeniBodovi}"
                    )
                    recentFragments.arguments = bundle
                    openFragment(recentFragments)
                }
                    return@OnNavigationItemSelectedListener true
            }
            R.id.zaustaviKviz -> {
                GlobalScope.launch (Dispatchers.Main) {
                    AccountRepository.postaviHash(AccountRepository.acHash)
                    PredmetIGrupaRepository.upisiGrupuUBazuPodataka()
                    PredmetIGrupaRepository.upisiPredmetUBazuPodataka()
                    KvizRepository.upisiKvizUBazuPodataka()
                    PitanjeKvizRepository.upisiPitanjeUBazuPodataka()
                    val recentFragments = FragmentKvizovi.newInstance()
                    openFragment(recentFragments)
                }
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNavigation= findViewById(R.id.bottomNav)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        val intentVarijavbla: Intent = this.intent
        KvizRepository.setContext(this)
        OdgovorRepository.setContext(this)
        AccountRepository.setContext(this)
        PredmetIGrupaRepository.setContext(this)
        PitanjeKvizRepository.setContext(this)
        GlobalScope.launch (Dispatchers.Main) {
            if (intentVarijavbla.data != null) intentVarijavbla.getStringExtra("payload")
                ?.let { AccountRepository.postaviHash(it) }
            else {
                AccountRepository.postaviHash(AccountRepository.acHash)
            }
            bottomNavigation.selectedItemId = R.id.kvizovi
            val kvizovi = FragmentKvizovi.newInstance()
            PredmetIGrupaRepository.upisiGrupuUBazuPodataka()
            PredmetIGrupaRepository.upisiPredmetUBazuPodataka()
            KvizRepository.upisiKvizUBazuPodataka()
            PitanjeKvizRepository.upisiPitanjeUBazuPodataka()
            openFragment(kvizovi)
        }
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.kontener, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onBackPressed() {
        val fragments = supportFragmentManager.fragments
        for (f in fragments) {
            if (f != null && f is FragmentKvizovi) f.onBackPressed()
            if (f != null && f is FragmentPredmeti) f.onBackPressed()
            if (f != null && f is FragmentPoruka) f.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(br, filter)
    }
    override fun onPause() {
        unregisterReceiver(br)
        super.onPause()
    }

}

