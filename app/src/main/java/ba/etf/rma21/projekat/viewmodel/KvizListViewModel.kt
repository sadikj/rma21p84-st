package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*

class KvizListViewModel {
    val scope = CoroutineScope(Job() + Dispatchers.Main)

    fun getAllKviz(onSuccess: (movies: List<Kviz>) -> Unit,
                             onError: () -> Unit){
        scope.launch{
            /*val result = KvizRepository.getAl1l().sortedBy { kviz -> kviz.datumPocetka}
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }*/
            if (DBRepository.updateNow()) {
                AccountRepository.updateDate(Date())
                PredmetIGrupaRepository.upisiGrupuUBazuPodataka()
                PredmetIGrupaRepository.upisiPredmetUBazuPodataka()
                KvizRepository.upisiKvizUBazuPodataka()
                PitanjeKvizRepository.upisiPitanjeUBazuPodataka()
                val result = KvizRepository.getAl1l().sortedBy { kviz -> kviz.datumPocetka}
                when (result) {
                    is List<Kviz> -> onSuccess?.invoke(result)
                    else-> onError?.invoke()
                }
            }
            else{
                val result = KvizRepository.getAl1l().sortedBy { kviz -> kviz.datumPocetka}
                when (result) {
                    is List<Kviz> -> onSuccess?.invoke(result)
                    else-> onError?.invoke()
                }
            }
        }
    }

    suspend fun getAllKviz1():List<Kviz>{
        return KvizRepository.getAl1l().sortedBy { kviz -> kviz.datumPocetka}
    }

    fun getDone(onSuccess: (movies: List<Kviz>) -> Unit,
                   onError: () -> Unit){
        scope.launch{
            /*val result = KvizRepository.getDone().sortedBy { kviz -> kviz.datumPocetka}
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }*/
            if (DBRepository.updateNow()) {
                AccountRepository.updateDate(Date())
                PredmetIGrupaRepository.upisiGrupuUBazuPodataka()
                PredmetIGrupaRepository.upisiPredmetUBazuPodataka()
                KvizRepository.upisiKvizUBazuPodataka()
                PitanjeKvizRepository.upisiPitanjeUBazuPodataka()
                val result = KvizRepository.getDone().sortedBy { kviz -> kviz.datumPocetka}
                when (result) {
                    is List<Kviz> -> onSuccess?.invoke(result)
                    else-> onError?.invoke()
                }
            }
            else{
                val result = KvizRepository.getDone().sortedBy { kviz -> kviz.datumPocetka}
                when (result) {
                    is List<Kviz> -> onSuccess?.invoke(result)
                    else-> onError?.invoke()
                }
            }
        }
    }

    suspend fun getDone1(): List<Kviz>{
        return KvizRepository.getDone().sortedBy { kviz -> kviz.datumPocetka }
    }

    fun getNotTaken(onSuccess: (movies: List<Kviz>) -> Unit,
                onError: () -> Unit){
        scope.launch{
            /*val result = KvizRepository.getNotTaken().sortedBy { kviz -> kviz.datumPocetka}
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }*/
            if (DBRepository.updateNow()) {
                AccountRepository.updateDate(Date())
                PredmetIGrupaRepository.upisiGrupuUBazuPodataka()
                PredmetIGrupaRepository.upisiPredmetUBazuPodataka()
                KvizRepository.upisiKvizUBazuPodataka()
                PitanjeKvizRepository.upisiPitanjeUBazuPodataka()
                val result = KvizRepository.getNotTaken().sortedBy { kviz -> kviz.datumPocetka}
                when (result) {
                    is List<Kviz> -> onSuccess?.invoke(result)
                    else-> onError?.invoke()
                }
            }
            else{
                val result = KvizRepository.getNotTaken().sortedBy { kviz -> kviz.datumPocetka}
                when (result) {
                    is List<Kviz> -> onSuccess?.invoke(result)
                    else-> onError?.invoke()
                }
            }
        }
    }

    suspend fun getNotTaken1(): List<Kviz>{
        return KvizRepository.getNotTaken().sortedBy { kviz -> kviz.datumPocetka }
    }

    fun getFuture(onSuccess: (movies: List<Kviz>) -> Unit,
                    onError: () -> Unit){
        scope.launch{
            /*val result = KvizRepository.getFuture().sortedBy { kviz -> kviz.datumPocetka}
            when (result) {
                is List<Kviz> -> onSuccess?.invoke(result)
                else-> onError?.invoke()
            }*/
            if (DBRepository.updateNow()) {
                AccountRepository.updateDate(Date())
                PredmetIGrupaRepository.upisiGrupuUBazuPodataka()
                PredmetIGrupaRepository.upisiPredmetUBazuPodataka()
                KvizRepository.upisiKvizUBazuPodataka()
                PitanjeKvizRepository.upisiPitanjeUBazuPodataka()
                val result = KvizRepository.getFuture().sortedBy { kviz -> kviz.datumPocetka}
                when (result) {
                    is List<Kviz> -> onSuccess?.invoke(result)
                    else-> onError?.invoke()
                }
            }
            else{
                val result = KvizRepository.getFuture().sortedBy { kviz -> kviz.datumPocetka}
                when (result) {
                    is List<Kviz> -> onSuccess?.invoke(result)
                    else-> onError?.invoke()
                }
            }
        }
    }

    suspend fun getFuture1(): List<Kviz>{
        return KvizRepository.getFuture().sortedBy { kviz -> kviz.datumPocetka  }
    }



    fun getMyKvizes(onSuccess: (movies: List<Kviz>) -> Unit,
                  onError: () -> Unit){
        scope.launch{
            if (DBRepository.updateNow()) {
                AccountRepository.updateDate(Date())
                PredmetIGrupaRepository.upisiGrupuUBazuPodataka()
                PredmetIGrupaRepository.upisiPredmetUBazuPodataka()
                KvizRepository.upisiKvizUBazuPodataka()
                PitanjeKvizRepository.upisiPitanjeUBazuPodataka()
                val result = KvizRepository.getMyKvizes().sortedBy { kviz -> kviz.datumPocetka}
                when (result) {
                    is List<Kviz> -> onSuccess?.invoke(result)
                    else-> onError?.invoke()
                }
            }
            else{
                val result = KvizRepository.getMyKvizes().sortedBy { kviz -> kviz.datumPocetka}
                when (result) {
                    is List<Kviz> -> onSuccess?.invoke(result)
                    else-> onError?.invoke()
                }
            }
        }
    }

    suspend fun getMyKvizes1():List<Kviz>{
        /*if (DBRepository.updateNow()) {
            AccountRepository.updateDate(Date())
            PredmetIGrupaRepository.upisiGrupuUBazuPodataka()
            KvizRepository.upisiKvizUBazuPodataka()
            return KvizRepository.getMyKvizes().sortedBy { kviz -> kviz.datumPocetka}
        }
        else{
            return KvizRepository.getMyKvizes().sortedBy { kviz -> kviz.datumPocetka}
        }*/
        return KvizRepository.getMyKvizes().sortedBy { kviz -> kviz.datumPocetka }
    }
}