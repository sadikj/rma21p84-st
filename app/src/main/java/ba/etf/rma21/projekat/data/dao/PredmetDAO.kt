package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Predmet

@Dao
interface PredmetDAO {
    @Query("SELECT * FROM predmet")
    suspend fun getPredmet() : List<Predmet>
    @Query("DELETE FROM predmet")
    suspend fun deleteAllPredmet()
    @Query("SELECT * FROM predmet WHERE godina = :godina")
    suspend fun getPredmetByGodina(godina:Int) : List<Predmet>
    @Insert
    suspend fun insertPredmet(vararg predmet: Predmet)
}