package ba.etf.rma21.projekat.data.models


import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class Kviz(
    @PrimaryKey @SerializedName("id") var id: Int,
    @ColumnInfo(name = "naziv") @SerializedName("naziv")  var naziv: String,
    @ColumnInfo(name = "datumPocetka") @SerializedName("datumPocetka")  var datumPocetka: String,
    @ColumnInfo(name = "datumKraj") @SerializedName("datumKraj")  var datumKraj: String?,
    @ColumnInfo(name = "trajanje") @SerializedName("trajanje")  var trajanje: Int,
    @ColumnInfo(name = "nazivPredmeta") @SerializedName("nazivPredmeta")  var nazivPredmeta: String,
    @ColumnInfo(name = "stanje") @SerializedName("stanje")  var stanje: String,
    @ColumnInfo(name = "datumRada") @SerializedName("datumRada")  var datumRada: String?,
    @ColumnInfo(name = "osvojeniBodovi") @SerializedName("osvojeniBodovi")  var osvojeniBodovi: Int?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Kviz

        if (id != other.id) return false
        if (naziv != other.naziv) return false
        if (datumPocetka != other.datumPocetka) return false
        if (datumKraj != other.datumKraj) return false
        if (trajanje != other.trajanje) return false
        if (nazivPredmeta != other.nazivPredmeta) return false
        if (stanje != other.stanje) return false
        if (datumRada != other.datumRada) return false
        if (osvojeniBodovi != other.osvojeniBodovi) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + naziv.hashCode()
        result = 31 * result + datumPocetka.hashCode()
        result = 31 * result + (datumKraj?.hashCode() ?: 0)
        result = 31 * result + trajanje
        result = 31 * result + nazivPredmeta.hashCode()
        result = 31 * result + stanje.hashCode()
        result = 31 * result + (datumRada?.hashCode() ?: 0)
        result = 31 * result + (osvojeniBodovi ?: 0)
        return result
    }
}

/*data class Kviz(val id:Int, val naziv: String, val datumPocetka: Date, val datumKraj: Date?, val trajanje: Int, val nazivPredmeta:String, val stanje:String, val datumRada:Date?, val osvojeniBodovi:Int?) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Kviz

        if (id != other.id) return false
        if (naziv != other.naziv) return false
        if (datumPocetka != other.datumPocetka) return false
        if (datumKraj != other.datumKraj) return false
        if (trajanje != other.trajanje) return false
        if (nazivPredmeta != other.nazivPredmeta) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + naziv.hashCode()
        result = 31 * result + datumPocetka.hashCode()
        result = 31 * result + (datumKraj?.hashCode() ?: 0)
        result = 31 * result + trajanje
        result = 31 * result + nazivPredmeta.hashCode()
        return result
    }
}*/
