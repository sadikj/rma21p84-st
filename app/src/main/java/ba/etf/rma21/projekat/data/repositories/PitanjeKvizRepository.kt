package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.*


class PitanjeKvizRepository {
    companion object {
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun getPitanja(idKviza: Int): List<Pitanje>? {
            return withContext(Dispatchers.IO) {
                try {
                    val pitanja = mutableListOf<Pitanje>()
                    val url1 = ApiConfig.baseURL + "/kviz/$idKviza/pitanja"
                    val url = URL(url1)
                    (url.openConnection() as? HttpURLConnection)?.run {
                        val result = this.inputStream.bufferedReader().use { it.readText() }
                        val jo = JSONArray(result)

                        for (i in 0 until jo.length()) {
                            val pitanje = jo.getJSONObject(i)
                            val opcijeNiz = pitanje.getJSONArray("opcije")
                            val opcije = mutableListOf<String>()
                            for (j in 0 until opcijeNiz.length()){
                                opcije.add(opcijeNiz.getString(j))
                            }
                            val naziv = pitanje.getString("naziv")
                            val tekstPitanja = pitanje.getString("tekstPitanja")
                            val tacan = pitanje.getInt("tacan")
                            val id =  pitanje.getInt("id")
                            var strinRezOpcije = ""
                            for(el in opcije){
                                strinRezOpcije += el.toString()+","
                            }
                            pitanja.add(Pitanje(id, naziv, tekstPitanja, strinRezOpcije, tacan,idKviza))
                        }
                    }
                    return@withContext pitanja
                } catch (e: MalformedURLException) {
                    return@withContext emptyList()
                }
            }
        }

        suspend fun upisiPitanjeUBazuPodataka(){
            try{
                //println("BBBBBBBBBBBBBBBBBBBBBBBBB")
                var db = AppDatabase.getInstance(context)
                db!!.pitanjeDAO().deleteAllFromPitanje()
                val kvizovi  = KvizRepository.dohvatiSveKvizoveIzBazePodataka()
                if(kvizovi!=null){
                    for(elKviz in kvizovi){
                        val pitanja =  getPitanja(elKviz.id)
                        if (pitanja != null) {
                            for (el in pitanja) {
                                db!!.pitanjeDAO().insertPitanje(el)
                            }
                        }
                    }
                }
            }
            catch(error:Exception){
            }
        }


        suspend fun dohvatiSvaPitanjaIzBazePodataka():List<Pitanje>{
            var db = AppDatabase.getInstance(context)
            val pitanje = db!!.pitanjeDAO().getPitanja()
            return  pitanje
        }

        suspend fun dohvatiSvaPitanjaIzBazePodatakaZaJedanKviz(idKviz: Int):List<Pitanje>{
            var db = AppDatabase.getInstance(context)
            val pitanje = db!!.pitanjeDAO().getAllQuestionsFromKviz(idKviz)
            return  pitanje
        }
    }

}
