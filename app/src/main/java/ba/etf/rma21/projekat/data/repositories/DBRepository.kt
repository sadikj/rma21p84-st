package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

class DBRepository {
    companion object{
        private lateinit var context:Context
        fun setContext(_context: Context){
            context=_context
        }


        suspend fun updateNow():Boolean{
            return withContext(Dispatchers.IO) {
                try {
                    var date = AccountRepository.datumUStringFormatu
                    var changed = false
                    val url1 = ApiConfig.baseURL+"/account/"+AccountRepository.acHash+"/lastUpdate?date="+date
                    val url = URL(url1)
                    (url.openConnection() as? HttpURLConnection)?.run {
                        val result = this.inputStream.bufferedReader().use { it.readText() }
                        val jo = JSONObject(result)
                        if (jo.has("message")) return@withContext false
                        changed = jo.getBoolean("changed")
                    }
                    return@withContext changed
                } catch (e: MalformedURLException) {
                    return@withContext false
                }
            }
        }


    }
}