package ba.etf.rma21.projekat

import androidx.appcompat.app.AppCompatActivity


class UpisPredmet : AppCompatActivity() {

    /*private val listaPredmeta : ArrayList<String> = ArrayList()
    private val listaGrupa : ArrayList<String> =  ArrayList()
    private var upisPredmetaViewModel =  UpisPredmetViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        var listaKorisnika = upisPredmetaViewModel.getAllKorsinik()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upis_predmet)

        val odabirGodina : Spinner = findViewById(R.id.odabirGodina)
        val odabirPredmet : Spinner = findViewById(R.id.odabirPredmet)
        val odabirGrupa : Spinner = findViewById(R.id.odabirGrupa)
        val dodajPredmetDugme : Button = findViewById(R.id.dodajPredmetDugme)


        val arrayList: ArrayList<String> = ArrayList()
        arrayList.add("1")
        arrayList.add("2")
        arrayList.add("3")
        arrayList.add("4")
        arrayList.add("5")
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, arrayList)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        odabirGodina.setAdapter(arrayAdapter)

        if (listaKorisnika != null && !listaKorisnika.isEmpty()){
            odabirGodina.setSelection(listaKorisnika[listaKorisnika.lastIndex].predmet.godina-1)
        }

        odabirGodina.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                listaPredmeta.clear()

                val predmeti : List<Predmet> = upisPredmetaViewModel.getPredmetsNotEnrolled(parent.selectedItemPosition+1)

                for(lista in predmeti){
                    listaPredmeta.add(lista.naziv)
                }

                popuniSpiner(odabirPredmet, listaPredmeta)

                if(odabirPredmet != null && odabirPredmet.getSelectedItem() !=null ) {
                    dodajPredmetDugme.isEnabled = true
                }
                else{
                    dodajPredmetDugme.isEnabled = false
                    listaGrupa.clear()
                    popuniSpiner(odabirGrupa, listaGrupa)
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        })

        odabirPredmet.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                listaGrupa.clear()
                if(!odabirPredmet.isEmpty()) {
                    val sveGrupe: List<Grupa> = upisPredmetaViewModel.getGroupsByPredmet(parentView.getItemAtPosition(position).toString())

                    for (lista in sveGrupe) {
                        listaGrupa.add(lista.naziv)
                    }

                    popuniSpiner(odabirGrupa, listaGrupa)
                }

                dodajPredmetDugme.isEnabled = odabirGrupa != null && odabirGrupa.getSelectedItem() !=null

            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here
            }
        })

        dodajPredmetDugme.setOnClickListener{
            val godina : Int = odabirGodina.selectedItemPosition+1
            val predmet : String = odabirPredmet.selectedItem.toString()
            val grupa : String = odabirGrupa.selectedItem.toString()
            upisPredmetaViewModel.setNewKorisnik(Predmet(predmet, godina), Grupa(grupa, predmet))
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

    }

    private fun popuniSpiner(odabir: Spinner, lista: ArrayList<String>) {
        val predmetAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, lista)
        predmetAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        odabir.setAdapter(predmetAdapter)
    }

     */

}