package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Pitanje

@Dao
interface PitanjeDAO {
    @Query("SELECT * FROM pitanje")
    suspend fun getPitanja(): List<Pitanje>

    @Query("DELETE FROM pitanje")
    suspend fun deleteAllFromPitanje()

    @Query("SELECT * FROM pitanje WHERE idKviz=:idKviz")
    suspend fun getAllQuestionsFromKviz(idKviz:Int):List<Pitanje>

    @Insert
    suspend fun insertPitanje(vararg pitanje: Pitanje)
}