package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Grupa

@Dao
interface GrupaDAO {
    @Query("SELECT * FROM grupa")
    suspend fun getGrupa(): List<Grupa>

    @Query("DELETE FROM grupa")
    suspend fun deleteAllFromGrupa()

    @Insert
    suspend fun insertGrupa(vararg grupa: Grupa)
}