package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.Predmet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.*
import javax.net.ssl.HttpsURLConnection


class PredmetIGrupaRepository {
        companion object{
            private lateinit var context: Context
            fun setContext(_context: Context){
                context=_context
            }
            suspend fun getPredmeti(): List<Predmet> {
                return withContext(Dispatchers.IO) {
                    try {
                        val predmeti = mutableListOf<Predmet>()
                        val url1 = ApiConfig.baseURL + "/predmet"
                        val url = URL(url1)
                        (url.openConnection() as? HttpURLConnection)?.run {
                            val result = this.inputStream.bufferedReader().use { it.readText() }
                            val jo = JSONArray(result)

                            for (i in 0 until jo.length()) {
                                val kviz = jo.getJSONObject(i)
                                val id = kviz.getInt("id")
                                val naziv = kviz.getString("naziv")
                                val godina = kviz.getInt("godina")
                                predmeti.add(Predmet(id,naziv,godina))
                            }
                        }
                        return@withContext predmeti
                    } catch (e: MalformedURLException) {
                        return@withContext emptyList()
                    }
                }
            }
            suspend fun getGrupe():List<Grupa>?{
                return withContext(Dispatchers.IO) {
                    try {
                        val grupe = mutableListOf<Grupa>()
                        val url1 = ApiConfig.baseURL + "/grupa"
                        val url = URL(url1)
                        (url.openConnection() as? HttpURLConnection)?.run {
                            val result = this.inputStream.bufferedReader().use { it.readText() }
                            val jo = JSONArray(result)

                            for (i in 0 until jo.length()) {
                                val kviz = jo.getJSONObject(i)
                                val id = kviz.getInt("id")
                                val naziv = kviz.getString("naziv")
                                val PredmetId = kviz.getInt("PredmetId")
                                grupe.add(Grupa(id,naziv,PredmetId))
                            }
                        }
                        return@withContext grupe
                    } catch (e: MalformedURLException) {
                        return@withContext emptyList()
                    }
                }
            }


            suspend fun upisiPredmetUBazuPodataka(){
                try{
                    //println("BBBBBBBBBBBBBBBBBBBBBBBBB")
                    var db = AppDatabase.getInstance(context)
                    db!!.predmetDAO().deleteAllPredmet()
                    val predmeti = getPredmeti()
                    if (predmeti != null) {
                        for (el in predmeti) {
                            db!!.predmetDAO().insertPredmet(el)
                        }
                    }
                }
                catch(error:Exception){
                }
            }


            suspend fun dohvatiSvePredmeteIzBazePodataka():List<Predmet>{
                var db = AppDatabase.getInstance(context)
                val predmet = db!!.predmetDAO().getPredmet()
                return  predmet
            }


            suspend fun dohvatiPredmetIzBazePodataka(godina:Int):List<Predmet>{
                var db = AppDatabase.getInstance(context)
                val predmet = db!!.predmetDAO().getPredmetByGodina(godina)
                return  predmet
            }


            suspend fun upisiGrupuUBazuPodataka(){
                try{
                    //println("BBBBBBBBBBBBBBBBBBBBBBBBB")
                    var db = AppDatabase.getInstance(PredmetIGrupaRepository.context)
                    db!!.grupaDAO().deleteAllFromGrupa()
                    val grupe = getUpisaneGrupe()
                    if (grupe != null) {
                        for (el in grupe) {
                            db!!.grupaDAO().insertGrupa(el)
                        }
                    }
                }
                catch(error:Exception){
                }
            }


            suspend fun dohvatiSveGrupeIzBazePodataka():List<Grupa>{
                var db = AppDatabase.getInstance(PredmetIGrupaRepository.context)
                val grupa = db!!.grupaDAO().getGrupa()
                return  grupa
            }



            suspend fun getGrupeZaPredmet(idPredmeta:Int):List<Grupa>?{
                return withContext(Dispatchers.IO) {
                    try {
                        val grupe = mutableListOf<Grupa>()
                        val url1 = ApiConfig.baseURL + "/predmet/${idPredmeta}/grupa"
                        val url = URL(url1)
                        (url.openConnection() as? HttpURLConnection)?.run {
                            val result = this.inputStream.bufferedReader().use { it.readText() }
                            val jo = JSONArray(result)

                            for (i in 0 until jo.length()) {
                                val kviz = jo.getJSONObject(i)
                                val id = kviz.getInt("id")
                                val naziv = kviz.getString("naziv")
                                val PredmetId = kviz.getInt("PredmetId")
                                grupe.add(Grupa(id,naziv,PredmetId))
                            }
                        }
                        return@withContext grupe
                    } catch (e: MalformedURLException) {
                        return@withContext emptyList()
                    }
                }
            }

            suspend fun upisiUGrupu(idGrupa:Int):Boolean?{
                return withContext(Dispatchers.IO) {
                    try {
                        var rezultat = false
                        val url1 = ApiConfig.baseURL+"/grupa/$idGrupa/student/"+AccountRepository.acHash
                        val url = URL(url1)
                        (url.openConnection() as? HttpsURLConnection)?.run {
                            requestMethod = "POST"
                            val result = this.inputStream.bufferedReader().use { it.readText() }
                            val jo = JSONObject(result)
                            val message = jo.getString("message")
                            val poruka = "Student"
                            if(message.contains(poruka)){
                                rezultat = true
                            }
                        }
                        return@withContext rezultat
                    } catch (e: MalformedURLException) {
                        return@withContext false
                    }
                }
            }



            suspend fun getPredmetsByGodina(godina:Int) : List<Predmet>{

                    val listaPredmeta = getPredmeti()
                    val rez = mutableListOf<Predmet>()
                    for(lista in listaPredmeta){
                        if(lista.godina==godina){
                            rez.add(lista)
                        }
                    }
                    return rez

            }

            suspend fun getPredmetsNotEnrolled(godina: Int): List<Predmet>{
                    val listaPred = mutableListOf<Predmet>()
                    val listaPredmeta : List<Predmet> = dohvatiPredmetIzBazePodataka(godina)
                    val upisaneGrupe = dohvatiSveGrupeIzBazePodataka()
                    for(predmet in listaPredmeta){
                        val grupePredmeta = getGrupeZaPredmet(predmet.id)
                        var postoji = false
                        for (elgP in grupePredmeta!!){
                            for (eluP in upisaneGrupe!!){
                                if (elgP.id == eluP.id){
                                    postoji = true
                                    break
                                }
                            }
                        }
                        if (!postoji){
                            listaPred.add(predmet)
                        }
                    }
                    return listaPred

            }

            suspend fun getUpisaneGrupe():List<Grupa>?{
                return withContext(Dispatchers.IO) {
                    try {
                        val grupe = mutableListOf<Grupa>()
                        val url1 = ApiConfig.baseURL + "/student/" + AccountRepository.acHash + "/grupa"
                        val url = URL(url1)
                        (url.openConnection() as? HttpURLConnection)?.run {
                            val result = this.inputStream.bufferedReader().use { it.readText() }
                            val jo = JSONArray(result)

                            for (i in 0 until jo.length()) {
                                val kviz = jo.getJSONObject(i)
                                val id = kviz.getInt("id")
                                val naziv = kviz.getString("naziv")
                                val PredmetId = kviz.getInt("PredmetId")
                                grupe.add(Grupa(id,naziv,PredmetId))
                            }
                        }
                        return@withContext grupe
                    } catch (e: MalformedURLException) {
                        return@withContext emptyList()
                    }
                }
            }

            suspend fun getPredmetNaziv(id:Int): String {
                return withContext(Dispatchers.IO) {
                    try {
                        var naziv = ""
                        val url1 = ApiConfig.baseURL + "/predmet/${id}"
                        val url = URL(url1)
                        val rezultat = (url.openConnection() as? HttpURLConnection)?.run {
                            val result = this.inputStream.bufferedReader().use { it.readText() }
                            val jo = JSONObject(result)
                            naziv = jo.getString("naziv")
                        }
                        return@withContext naziv
                    } catch (e: MalformedURLException) {
                        return@withContext ""
                    }
                }
            }

            suspend fun getNazivPredmetKviz(kvizId:Int):String{
                return withContext(Dispatchers.IO) {
                    try {
                        var naziv = ""
                        val url1 = ApiConfig.baseURL + "/kviz/${kvizId}/grupa"
                        val url = URL(url1)
                        val rezultat = (url.openConnection() as? HttpURLConnection)?.run {
                            val result = this.inputStream.bufferedReader().use { it.readText() }
                            val jo = JSONArray(result)
                            //provjera da li je array prazan ukoliko nije da dodamo ga u naziv
                            //metoda vraca naziv premeta kao pomocna za lakse pristupimo podacima u kvizu
                            if (jo.length()>0) {
                                val JSONobjekat = jo.getJSONObject(0)
                                val id = JSONobjekat.getInt("PredmetId")
                                naziv = getPredmetNaziv(id)
                            }
                        }
                        return@withContext naziv
                    } catch (e: MalformedURLException) {
                        return@withContext ""
                    }
                }
            }
        }
}