package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Kviz
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class KvizRepository {

    //DATUM POPRAVITI

    companion object {
        var datumRada : String? = ""
        var osvojeniBodovi : Int? = null
        init {

        }
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }

        suspend fun getMyKvizes(): List<Kviz> {
            //return getUpisani()!!
            return dohvatiSveKvizoveIzBazePodataka()
        }

        suspend fun getAl1l(): List<Kviz> {
            return getAll()!!
        }

        //moje kvizove i zapocete i provjeravam id
        suspend fun getDone(): List<Kviz> {
            val sviKvizovi = getMyKvizes()
            val zapocetiKvizovi = TakeKvizRepository.getPocetiKvizovi()
            val listaReturn = mutableListOf<Kviz>()
            if (zapocetiKvizovi != null ) {
                for (elMojiKv in sviKvizovi) {
                    for (elZapKv in zapocetiKvizovi) {
                        if (elMojiKv.id == elZapKv.KvizId) {
                            listaReturn.add(elMojiKv)
                        }
                    }
                }
            }
            return listaReturn
        }


        suspend fun getFuture(): List<Kviz> {
            val sviKvizovi = getAll()
            val listaReturn = mutableListOf<Kviz>()
            for(elListe in sviKvizovi!!){
                var datum = vratiDatumIzStringa(elListe.datumPocetka)
                if(datum.after(Date())){
                    listaReturn.add(elListe)
                }
            }
            return listaReturn
        }

        fun vratiDatumIzStringa(stringDatum: String) : Date{
            var splitPoT = stringDatum.split("T")
            var splitPoCrticama = splitPoT[0].split("-")
            val rezultatGregorian = GregorianCalendar(splitPoCrticama[0].toInt(), splitPoCrticama[1].toInt()-1, splitPoCrticama[2].toInt())
            val rezultat = Date(rezultatGregorian.timeInMillis)
            return rezultat
        }

        suspend fun getNotTaken(): List<Kviz> {
            val sviMojiKvizovi = getMyKvizes()
            val sviDoneKvizovi = getDone()
            val rezultat = mutableListOf<Kviz>()
            var provjeri = false
            for(elMojiKv in sviMojiKvizovi){
                for(elDoneKv in sviDoneKvizovi){
                    if(elMojiKv == elDoneKv){
                        provjeri = true
                    }
                }
                if(!provjeri && Date().after(vratiDatumIzStringa(elMojiKv.datumKraj!!))){
                    rezultat.add(elMojiKv)
                }
                provjeri = false
            }
            return rezultat
        }

        suspend fun getAll(): List<Kviz>? {
            return withContext(Dispatchers.IO) {
                try {
                    val kvizovi = mutableListOf<Kviz>()
                    datumRada = ""
                    osvojeniBodovi = null
                    val url1 = ApiConfig.baseURL + "/kviz"
                    val url = URL(url1)
                    (url.openConnection() as? HttpURLConnection)?.run {
                        val result = this.inputStream.bufferedReader().use { it.readText() }
                        val jo = JSONArray(result)

                        for (i in 0 until jo.length()) {
                            val kviz = jo.getJSONObject(i)
                            val id = kviz.getInt("id")
                            val naziv = kviz.getString("naziv")
                            val trajanje = kviz.getInt("trajanje")
                            val nazivPredmet = PredmetIGrupaRepository.getNazivPredmetKviz(id)

                            val datumPocetakUStringFormatu: String = kviz.get("datumPocetak") as String
                            val datumPocetak = Date(getMilliFromDate(datumPocetakUStringFormatu))
                            if (kviz.get("datumKraj").toString() == "null") {
                                val stanje = odrediStanje(id)
                                kvizovi.add(
                                    Kviz(
                                        id,
                                        naziv,
                                        datumPocetakUStringFormatu + "T00:00:00",
                                        "2100-05-05T00:00:00",
                                        trajanje,
                                        nazivPredmet,
                                        stanje,
                                        datumRada,
                                        osvojeniBodovi
                                    )
                                )
                            }else{
                                val stanje = odrediStanje(id)
                                val datumKrajUStringFormatu : String = kviz.get("datumKraj") as String
                                val datumKraj = Date(getMilliFromDate(datumKrajUStringFormatu))
                                kvizovi.add(Kviz(id, naziv, datumKrajUStringFormatu+"T00:00:00", datumKrajUStringFormatu+"T00:00:00", trajanje,nazivPredmet,stanje,
                                    datumRada,
                                    osvojeniBodovi))
                            }
                        }
                    }
                    return@withContext kvizovi
                } catch (e: MalformedURLException) {
                    return@withContext emptyList()
                }
            }
        }

        suspend fun getById(id: Int): Kviz? {
            return withContext(Dispatchers.IO) {
                try {
                    var kvizRezultat : Kviz? = null
                    val url1 = ApiConfig.baseURL + "/kviz/$id"
                    val url = URL(url1)
                    val run = (url.openConnection() as? HttpURLConnection)?.run {
                        val result = this.inputStream.bufferedReader().use { it.readText() }
                        val jo = JSONObject(result)
                        val id = jo.getInt("id")
                        val naziv = jo.getString("naziv")
                        val trajanje = jo.getInt("trajanje")
                        val datumPocetakUStringFormatu: String = jo.get("datumPocetak") as String
                        val datumPocetak = Date(getMilliFromDate(datumPocetakUStringFormatu))
                        val nazivPredmet = PredmetIGrupaRepository.getNazivPredmetKviz(id)
                        if (jo.get("datumKraj").toString() == "null")
                            kvizRezultat = Kviz(id, naziv, datumPocetakUStringFormatu+"T00:00:00", "2100-05-05T00:00:00", trajanje,nazivPredmet,"",
                                datumRada,
                                osvojeniBodovi)
                        else{
                            val datumKrajUStringFormatu : String = jo.get("datumKraj") as String
                            val datumKraj = Date(getMilliFromDate(datumKrajUStringFormatu))
                            kvizRezultat = Kviz(id, naziv, datumPocetakUStringFormatu+"T00:00:00", datumKrajUStringFormatu+"T00:00:00", trajanje,nazivPredmet,"",
                                datumRada,
                                osvojeniBodovi)
                        }
                    }
                    return@withContext kvizRezultat
                } catch (e: MalformedURLException) {
                    return@withContext null
                }
            }
        }

        suspend fun upisiKvizUBazuPodataka(){
            try{
                //println("BBBBBBBBBBBBBBBBBBBBBBBBB")
                var db = AppDatabase.getInstance(context)
                db!!.kvizDAO().deleteAllFromKviz()
                val kvizovi = getUpisani()!!
                for (el in kvizovi) {
                    db!!.kvizDAO().insertKviz(el)
                }
            }
            catch(error:Exception){
            }
        }


        suspend fun dohvatiSveKvizoveIzBazePodataka():List<Kviz>{
            var db = AppDatabase.getInstance(context)
            val kvizovi = db!!.kvizDAO().getKviz()
            return  kvizovi
        }

        suspend fun getUpisani(): List<Kviz>? {
            return withContext(Dispatchers.IO) {
                try {
                    val sveMojeGrupe = PredmetIGrupaRepository.dohvatiSveGrupeIzBazePodataka()
                    val sviMojiKvizovi = mutableListOf<Kviz>()
                    datumRada = ""
                    osvojeniBodovi = null
                    for (i in 0..sveMojeGrupe!!.size-1) {
                        val url1 = ApiConfig.baseURL + "/grupa/" + sveMojeGrupe[i].id + "/kvizovi"
                        val url = URL(url1)
                        (url.openConnection() as? HttpURLConnection)?.run {
                            val result = this.inputStream.bufferedReader().use { it.readText() }
                            val jo = JSONArray(result)

                            for (j in 0 until jo.length()) {
                                val kviz = jo.getJSONObject(j)
                                val id = kviz.getInt("id")
                                val naziv = kviz.getString("naziv")
                                val trajanje = kviz.getInt("trajanje")
                                val datumPocetakUStringFormatu: String = kviz.get("datumPocetak") as String
                                val datumPocetak = Date(getMilliFromDate(datumPocetakUStringFormatu))
                                val nazivPredmet = PredmetIGrupaRepository.getPredmetNaziv(sveMojeGrupe[i].idPredmet)
                                if (kviz.get("datumKraj").toString() == "null") {
                                    val stanje = odrediStanje(id)
                                    sviMojiKvizovi.add(
                                        Kviz(
                                            id,
                                            naziv,
                                            datumPocetakUStringFormatu+"T00:00:00",
                                            "2100-05-05T00:00:00",
                                            trajanje,
                                            nazivPredmet,
                                            stanje,
                                            datumRada,
                                            osvojeniBodovi)
                                    )
                                }
                                    else{
                                    val stanje = odrediStanje(id)
                                    val datumKrajUStringFormatu : String = kviz.get("datumKraj") as String
                                    val datumKraj = Date(getMilliFromDate(datumKrajUStringFormatu))
                                    sviMojiKvizovi.add(Kviz(id, naziv, datumPocetakUStringFormatu+"T00:00:00", datumKrajUStringFormatu+"T00:00:00", trajanje,nazivPredmet, stanje,
                                        datumRada,
                                        osvojeniBodovi))
                                }
                            }
                        }
                    }
                    return@withContext sviMojiKvizovi
                } catch (e: MalformedURLException) {
                    return@withContext emptyList()
                }
            }
        }

        suspend fun odrediStanje(kvizId:Int):String{
            val zapocetiKvizovi = TakeKvizRepository.getPocetiKvizovi()
            if (zapocetiKvizovi != null) {
                for (el in zapocetiKvizovi) {
                    if (kvizId == el.KvizId){
                        val patternDate = "yyyy-MM-dd HH:mm:ss"
                        val simpleDateFormat = SimpleDateFormat(patternDate)
                        datumRada = simpleDateFormat.format(el.datumRada).split(" ")[0]+"T"+simpleDateFormat.format(el.datumRada).split(" ")[1]
                        osvojeniBodovi = el.osvojeniBodovi
                        return "zavrsen"
                    }
                }
            }
            val kviz = getById(kvizId)
            if (kviz != null){
                if (vratiDatumIzStringa(kviz.datumKraj!!).before(Date())) return "prosli"
                if (vratiDatumIzStringa(kviz.datumPocetka).after(Date())) return "buduci"
                if (vratiDatumIzStringa(kviz.datumPocetka).before(Date()) && vratiDatumIzStringa(kviz.datumKraj!!).after(Date())) return "aktivan"
            }
            return ""
        }

        fun getMilliFromDate(dateFormat: String?): Long {
            var date = Date()
            val formatter = SimpleDateFormat("yyyy-MM-dd")
            try {
                date = formatter.parse(dateFormat)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return date.time
        }


    }
}