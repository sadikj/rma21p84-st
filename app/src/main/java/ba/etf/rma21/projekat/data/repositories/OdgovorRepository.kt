package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Odgovor
import ba.etf.rma21.projekat.data.models.Pitanje
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.*
import java.util.*


class OdgovorRepository {
    companion object {
        var brojac  = 0
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        suspend fun getOdgovoriKviz(idKviza: Int): List<Odgovor>? {
            return withContext(Dispatchers.IO) {
                try {
                    var idKvizTeken = TakeKvizRepository.getPocetiKvizovi()
                    var idRez = 0
                    for(el in idKvizTeken!!){
                        if(el.KvizId == idKviza){
                            idRez = el.id
                        }
                    }
                    val odgovori = mutableListOf<Odgovor>()
                    val url1 = ApiConfig.baseURL + "/student/"+AccountRepository.acHash+"/kviztaken/${idRez}/odgovori"
                    val url = URL(url1)
                    (url.openConnection() as? HttpURLConnection)?.run {
                        val result = this.inputStream.bufferedReader().use { it.readText() }
                        val jo = JSONArray(result)

                        for (i in 0 until jo.length()) {
                            val odgovor = jo.getJSONObject(i)
                            val odgovoreno = odgovor.getInt("odgovoreno")
                            val KvizTakenId = odgovor.getInt("KvizTakenId")
                            val PitanjeId = odgovor.getInt("PitanjeId")
                            odgovori.add(Odgovor(brojac, odgovoreno, KvizTakenId, PitanjeId, idKviza
                            ))
                            brojac+=1
                        }
                    }
                    return@withContext odgovori
                } catch (e: MalformedURLException) {
                    return@withContext emptyList()
                }
            }
        }

        suspend fun dohvatiSveOdgovoreZaKvizTakenIzBazePodataka(idKviz: Int):List<Odgovor>{
            var db = AppDatabase.getInstance(context)
            var idKvizTeken = TakeKvizRepository.getPocetiKvizovi()
            var idRez = 0
            for(el in idKvizTeken!!){
                if(el.KvizId == idKviz){
                    idRez = el.id
                }
            }
            val odgovori = db!!.odgovorDAO().getOdgovorZaKvizTakenId(idRez)
            return  odgovori
        }

        suspend fun postaviOdgovorKviz(idKvizTaken: Int, idPitanje: Int, odgovor: Int): Int? {
            return withContext(Dispatchers.IO) {
                try {
                    //+ako nema kviza vrati -1+
                    var zapocetiKvizovi = TakeKvizRepository.getPocetiKvizovi()
                    var pamtiKviz : KvizTaken? = null
                    for(el in zapocetiKvizovi!!){
                        if(el.id == idKvizTaken){
                            pamtiKviz = el
                        }
                    }
                    var bodoviKviz = -1
                    if(pamtiKviz!=null) {
                        bodoviKviz = pamtiKviz!!.osvojeniBodovi
                        var svaPitanja = PitanjeKvizRepository.getPitanja(pamtiKviz!!.KvizId)

                        var zapamtiOdgovor = false
                        for (el in svaPitanja!!) {
                            if (el.id == idPitanje) {
                                if (el.tacan == odgovor) {
                                    zapamtiOdgovor = true
                                }
                            }
                        }

                        if (zapamtiOdgovor) {
                            bodoviKviz += 100 / svaPitanja.size
                        }
                    }

                    val odgovorObjekat = Odgovor((idPitanje*100)+idKvizTaken, odgovor, idKvizTaken, idPitanje,
                        pamtiKviz!!.KvizId)

                    var db = AppDatabase.getInstance(context)
                    if (db.odgovorDAO().getOdgovor(idKvizTaken, idPitanje) == 0)
                        db!!.odgovorDAO().insertAll(odgovorObjekat)

                        /*val url1 =
                            ApiConfig.baseURL + "/student/" + AccountRepository.acHash + "/kviztaken/${idKvizTaken}/odgovor"
                        val url = URL(url1)
                        val con = url.openConnection() as HttpURLConnection
                        con.doOutput = true
                        con.doInput = true
                        con.setRequestProperty("Content-Type", "application/json")
                        con.setRequestProperty("Accept", "application/json")
                        con.requestMethod = "POST"

                        val cred = JSONObject()

                        cred.put("odgovor", odgovor)
                        cred.put("pitanje", idPitanje)
                        cred.put("bodovi", bodoviKviz)

                        val wr = OutputStreamWriter(con.outputStream)
                        wr.write(cred.toString())
                        wr.flush()
                        val HttpResult = con.responseCode
                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                            val br = BufferedReader(InputStreamReader(con.inputStream, "utf-8"))
                            br.close()
                        } else {

                        }*/

                    return@withContext bodoviKviz
                } catch (e: MalformedURLException) {
                    return@withContext null
                }
            }
        }

    }


}