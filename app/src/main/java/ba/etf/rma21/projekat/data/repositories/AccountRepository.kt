package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.Account
import android.content.Context
import ba.etf.rma21.projekat.data.AppDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*


class AccountRepository {

    companion object {
        //TODO Ovdje trebate dodati hash string vašeg accounta
        var acHash: String = "717a3f8c-7ec5-44ca-bcef-49d2f1a72457"
        var datumUStringFormatu = ""
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        /*fun postaviHash(acHash: String): Boolean {
            this.acHash = acHash
            return true
        }*/
         fun updateDate(date:Date){
            val patternDate = "yyyy-MM-dd HH:mm:ss"
            val simpleDateFormat = SimpleDateFormat(patternDate)
            var dateResult = simpleDateFormat.format(date).split(" ")[0]+"T"+simpleDateFormat.format(date).split(" ")[1]
             datumUStringFormatu= dateResult
            try{
                val db = AppDatabase.getInstance(context)
                db!!.accountDao().updateDateOnAccount(acHash, datumUStringFormatu)
            }
            catch(error:Exception){
            }
         }

        suspend fun postaviHash(acHash: String): Boolean {
            this.acHash = acHash
            val patternDate = "yyyy-MM-dd HH:mm:ss"
            val simpleDateFormat = SimpleDateFormat(patternDate)
            var date = simpleDateFormat.format(Date()).split(" ")[0]+"T"+simpleDateFormat.format(Date()).split(" ")[1]
            datumUStringFormatu = date
            return withContext(Dispatchers.IO) {
                try{
                    val db = AppDatabase.getInstance(context)
                    db!!.clearAllTables()

                    db!!.accountDao().insertAccount(Account(acHash, datumUStringFormatu))
                    return@withContext true
                }
                catch(error:Exception){
                    return@withContext false
                }
            }
        }

        fun getHash(): String {
            return acHash
        }


    }
}