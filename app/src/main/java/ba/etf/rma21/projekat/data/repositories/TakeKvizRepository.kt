package ba.etf.rma21.projekat.data.repositories

import android.content.Context
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.*
import javax.net.ssl.HttpsURLConnection

class TakeKvizRepository {
    companion object {
        private lateinit var context: Context
        fun setContext(_context: Context){
            context=_context
        }
        //DATUM POPRAVITI
        suspend fun zapocniKviz(idKviza: Int): KvizTaken? {
            return withContext(Dispatchers.IO) {
                try {
                    var kvizTakenRezultat : KvizTaken? = null
                    val url1 = ApiConfig.baseURL + "/student/"+ AccountRepository.acHash +"/kviz/${idKviza}"
                    val url = URL(url1)
                    (url.openConnection() as? HttpsURLConnection)?.run {
                        requestMethod = "POST"
                        val result = this.inputStream.bufferedReader().use { it.readText() }
                        val jo = JSONObject(result)
                        if(!jo.toString().contains("message")) {
                            val id = jo.getInt("id")
                            val student = jo.getString("student")
                            var datumRada = Date()
                            val osvojeniBodovi = jo.getInt("osvojeniBodovi")
                            if (jo.get("datumRada").toString() == "null"){
                                kvizTakenRezultat = KvizTaken(id, idKviza, student, null, osvojeniBodovi)
                            }
                            else{
                                val datumRadaUStringFormatu : String = jo.get("datumRada") as String
                                datumRada = Date(
                                    KvizRepository.getMilliFromDate(
                                        datumRadaUStringFormatu
                                    )
                                )
                                kvizTakenRezultat = KvizTaken(id, idKviza, student, datumRada, osvojeniBodovi)
                            }
                        }

                    }
                    return@withContext kvizTakenRezultat
                } catch (e: MalformedURLException) {
                    return@withContext null
                }
            }
        }

        //DATUM + KVIZ ID PROVJERITI
        suspend fun getPocetiKvizovi(): List<KvizTaken>? {
            return withContext(Dispatchers.IO) {
                try {
                    val kvizovi = mutableListOf<KvizTaken>()
                    val url1 = ApiConfig.baseURL + "/student/" + AccountRepository.acHash + "/kviztaken"
                    val url = URL(url1)
                    (url.openConnection() as? HttpURLConnection)?.run {
                        val result = this.inputStream.bufferedReader().use { it.readText() }
                        val jo = JSONArray(result)

                        for (i in 0 until jo.length()) {
                            val kviz = jo.getJSONObject(i)
                            val id = kviz.getInt("id")
                            val student = kviz.getString("student")
                            val kvizId = kviz.getInt("KvizId")
                            val osvojeniBodovi = kviz.getInt("osvojeniBodovi")
                            var datumRada = Date()
                            if (kviz.get("datumRada").toString() == "null") {
                                kvizovi.add(KvizTaken(id, kvizId,student,null ,osvojeniBodovi))
                            }
                            else {
                                val datumRadaUStringFormatu : String = kviz.get("datumRada") as String
                                datumRada = Date(
                                    KvizRepository.getMilliFromDate(
                                        datumRadaUStringFormatu
                                    )
                                )
                                kvizovi.add(
                                    KvizTaken(
                                        id,
                                        kvizId,
                                        student,
                                        datumRada,
                                        osvojeniBodovi
                                    )
                                )
                            }
                        }
                        if(kvizovi.isEmpty()){
                            return@withContext null
                        }
                        else{
                            return@withContext kvizovi
                        }
                    }
                    //return@withContext kvizovi
                } catch (e: MalformedURLException) {
                    return@withContext null
                }
            }
        }
    }

}

