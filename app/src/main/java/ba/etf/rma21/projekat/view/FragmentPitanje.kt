package ba.etf.rma21.projekat.view

import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.models.KvizTaken
import ba.etf.rma21.projekat.data.models.Pitanje
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository
import ba.etf.rma21.projekat.data.repositories.OdgovorRepository.Companion.postaviOdgovorKviz
import com.google.android.material.navigation.NavigationView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class FragmentPitanje(pitanje1: Pitanje) : Fragment() {
    var pitanje1 : Pitanje = pitanje1
    lateinit var tekstPitanja : TextView
    lateinit var odgovoriLista : ListView
    var spasiBroj : Int = 0
    var kvizTaken : KvizTaken = FragmentKvizovi.idKvizaTakena
    var kviz : Kviz = FragmentKvizovi.kviz1Glo

    override  fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_pitanje, container, false)
        spasiBroj = FragmentPokusaj.spasiBroj
        tekstPitanja = view.findViewById(R.id.tekstPitanja)
        odgovoriLista = view.findViewById(R.id.odgovoriLista)
        tekstPitanja.text = pitanje1.tekstPitanja
        /*var listaOdgovora = mutableListOf<String>()
        var splitOpcije = pitanje1.opcije.split(",")
        for(el in splitOpcije){
            listaOdgovora.add(el)
        }*/
        odgovoriLista.adapter = context?.let { ArrayAdapter(it, android.R.layout.simple_list_item_1, pitanje1.opcije.split(",")) }
        var navigationBar : NavigationView? = parentFragment?.requireView()?.findViewById(R.id.navigacijaPitanja)
        var listaOdgovoraKorisnika = FragmentPokusaj.getListaOdgovora()
        //var listaPitanjeKvizOdgovo = PitanjeKvizRepository1.listaSvega
        var kvizGlobal = FragmentKvizovi.kviz1Glo

        var brojac = 0
        /*if(listaOdgovoraKorisnika!=null) {
            for (elListe in listaPitanjeKvizOdgovo) {
                if (elListe.kviz1 == kvizGlobal){
                    break
                }
                brojac++
            }
        }*/

        odgovoriLista.post {
            /*if(listaOdgovoraKorisnika[spasiBroj]==-2){
                odgovoriLista.isEnabled = false
            }
            else if(listaOdgovoraKorisnika[spasiBroj]!=-1 ){
                odgovoriLista.isEnabled = false
                if(listaOdgovoraKorisnika[spasiBroj] == pitanje1.tacan){
                    val tacan = odgovoriLista.getChildAt(listaOdgovoraKorisnika[spasiBroj]) as TextView
                    tacan.setTextColor(Color.parseColor("#3DDC84"))
                    val bojaTExtaPozadine = SpannableString(navigationBar?.menu?.getItem(spasiBroj)?.getTitle().toString())
                    if (container != null) {
                        bojaTExtaPozadine.setSpan(ForegroundColorSpan(ContextCompat.getColor(container.context, R.color.zelena)), 0, bojaTExtaPozadine.length, 0)
                    }
                    navigationBar?.menu?.getItem(spasiBroj)?.title = bojaTExtaPozadine
                    //procenatTacnosti+=1
                }
                else{
                    val bojaTExtaPozadine = SpannableString(navigationBar?.menu?.getItem(spasiBroj)?.getTitle().toString())
                    if (container != null) {
                        bojaTExtaPozadine.setSpan(ForegroundColorSpan(ContextCompat.getColor(container.context, R.color.crvena)), 0, bojaTExtaPozadine.length, 0)
                    }
                    navigationBar?.menu?.getItem(spasiBroj)?.title = bojaTExtaPozadine
                    odgovoriLista.getChildAt(listaOdgovoraKorisnika[spasiBroj]).setBackgroundColor(Color.parseColor("#DB4F3D"))
                    odgovoriLista.getChildAt(pitanje1.tacan).setBackgroundColor(Color.parseColor("#3DDC84"))
                }
            }
            else if(FragmentKvizovi.provjera == true){
                odgovoriLista.isEnabled = false
            }*/
            GlobalScope.launch (Dispatchers.Main) {
                var listaOdg = OdgovorRepository.dohvatiSveOdgovoreZaKvizTakenIzBazePodataka(kviz.id)
                if (listaOdg != null) {
                    for(elListe in listaOdg){
                        if(elListe.PitanjeId == pitanje1.id && elListe.KvizTakenId == kvizTaken.id){
                            if(elListe.odgovoreno==pitanje1.tacan){
                                val tacan = odgovoriLista.getChildAt(elListe.odgovoreno) as TextView
                                tacan.setTextColor(Color.parseColor("#3DDC84"))
                                val bojaTExtaPozadine = SpannableString(navigationBar?.menu?.getItem(spasiBroj)?.getTitle().toString())
                                if (container != null) {
                                    bojaTExtaPozadine.setSpan(ForegroundColorSpan(ContextCompat.getColor(container.context, R.color.zelena)), 0, bojaTExtaPozadine.length, 0)
                                }
                                navigationBar?.menu?.getItem(spasiBroj)?.title = bojaTExtaPozadine
                            }
                            else{
                                val bojaTExtaPozadine = SpannableString(navigationBar?.menu?.getItem(spasiBroj)?.getTitle().toString())
                                if (container != null) {
                                    bojaTExtaPozadine.setSpan(ForegroundColorSpan(ContextCompat.getColor(container.context, R.color.crvena)), 0, bojaTExtaPozadine.length, 0)
                                }
                                navigationBar?.menu?.getItem(spasiBroj)?.title = bojaTExtaPozadine
                                odgovoriLista.getChildAt(elListe.odgovoreno).setBackgroundColor(Color.parseColor("#DB4F3D"))
                                odgovoriLista.getChildAt(pitanje1.tacan).setBackgroundColor(Color.parseColor("#3DDC84"))
                            }
                            odgovoriLista.isEnabled = false
                        }
                    }
                }
            }

        }


        odgovoriLista.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            GlobalScope.launch (Dispatchers.Main) {
            if (position == pitanje1.tacan) {
                //procenatTacnosti += 1
                val bojaTExtaPozadine =
                    SpannableString(navigationBar?.menu?.getItem(spasiBroj)?.getTitle().toString())
                if (container != null) {
                    bojaTExtaPozadine.setSpan(
                        ForegroundColorSpan(
                            ContextCompat.getColor(
                                container.context,
                                R.color.zelena
                            )
                        ), 0, bojaTExtaPozadine.length, 0
                    )
                }

                navigationBar?.menu?.getItem(spasiBroj)?.title = bojaTExtaPozadine

                val tacan = view.findViewById(android.R.id.text1) as TextView
                tacan.setTextColor(Color.parseColor("#3DDC84"))
                odgovoriLista.isEnabled = false
                //listaOdgovoraKorisnika[spasiBroj] = position
                /*if(listaPitanjeKvizOdgovo!=null){
                        listaPitanjeKvizOdgovo[brojac].listaOdgovora[spasiBroj] = position
                    }*/

            } else {
                val bojaTExtaPozadine =
                    SpannableString(navigationBar?.menu?.getItem(spasiBroj)?.getTitle().toString())
                if (container != null) {
                    bojaTExtaPozadine.setSpan(
                        ForegroundColorSpan(
                            ContextCompat.getColor(
                                container.context,
                                R.color.crvena
                            )
                        ), 0, bojaTExtaPozadine.length, 0
                    )
                }
                navigationBar?.menu?.getItem(spasiBroj)?.title = bojaTExtaPozadine

                odgovoriLista.getChildAt(position).setBackgroundColor(Color.parseColor("#DB4F3D"))
                odgovoriLista.getChildAt(pitanje1.tacan)
                    .setBackgroundColor(Color.parseColor("#3DDC84"))
                odgovoriLista.isEnabled = false
                listaOdgovoraKorisnika[spasiBroj] = position
                /*if(listaPitanjeKvizOdgovo!=null){
                        listaPitanjeKvizOdgovo[brojac].listaOdgovora[spasiBroj] = position
                    }*/
            }
            postaviOdgovorKviz(kvizTaken.id, pitanje1.id, position)
        }        }


        return view
    }


    companion object {
        var procenatTacnosti : Int = 0
        fun newInstance(pitanje1: Pitanje): FragmentPitanje = FragmentPitanje(pitanje1)
    }

}