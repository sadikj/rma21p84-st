package ba.etf.rma21.projekat.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import ba.etf.rma21.projekat.data.models.Odgovor

@Dao
interface OdgovorDAO {
    @Query("SELECT COUNT(id) FROM odgovor WHERE KvizTakenId = :kvizTakenId AND PitanjeId = :pitanjeId")
    suspend fun getOdgovor(kvizTakenId:Int, pitanjeId:Int):Int

    @Query("SELECT * FROM odgovor WHERE KvizTakenId = :kvizTakenId")
    suspend fun getOdgovorZaKvizTakenId(kvizTakenId:Int):List<Odgovor>

    @Insert
    suspend fun insertAll(vararg odgovor: Odgovor)
}